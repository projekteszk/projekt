# Pizza Projekt#
https://eltepizza.herokuapp.com/

### rövid leírás ###

A projekt egy webes alkalmazás amin keresztül pizzákat lehet összeállítani.
Az oldal a szerverrel rest webservice-en keresztül kommunikál. Az szerveren történik a pizzák hitelesítése, fájlban tárolása és az ár kiszámolása.

### fejlesztéshez ###

A projekt továbbfejlesztéséhez szükséges:
* Java jdk : http://openjdk.java.net/
* Apache Maven : https://maven.apache.org/
* Apache Subversion : https://subversion.apache.org/ vagy git : https://git-scm.com/

### futtatáshoz ###

A Pizza könyvtár gyökerében állva adjuk ki az 'mvn compile' parancsot, ez elkészíti a futtatható állományt. A webszerver lokális indításához, ugyaninnen adjuk ki a target\bin\pizza parancsot (windows)

### felhasznált technológiák / dependenciák ###

webszerver : embedded Apache Tomcat 7
webservices : Jersey
unit test : JUnit 
json parsing : Jackson

