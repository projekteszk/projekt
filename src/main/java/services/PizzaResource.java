package services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import core.Pizza;
import core.PizzaStore;
import java.io.IOException;
 
@Path("/p")
public class PizzaResource {
 
    @GET @Path("test")
    @Produces("text/plain")
    public String test() {
        return "REST service working";
    }

    @POST @Path("add")
    @Consumes("application/json") @Produces("application/json")
    public String add(Pizza pizza) throws IOException {
        System.out.println("Pizza request érkezett: " + pizza.toString());
        if(pizza.isValid()){
            //PizzaStore.savePizza(pizza); <- nem működik
            //@TODO kiszámolni összesen mennyi a fájlban tárolt pizzák "ára"
            Integer price = PizzaStore.calculate(pizza);

            return "{\"status\" : \"ok\", \"price\" : "+ price +" }"; 
        }
        else return "{\"status\" : \"invalid\"}";
    }
}