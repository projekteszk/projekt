package misc;

import java.util.ArrayList;

public class Utils{

	/**
	 * Eldönti, hogy egy tömbben megtalálható-e egy adott elem
	 * @param <T> a tömb és az elem típusa
	 * @param array a tömb, amiben az elemet kell keresni
	 * @param element a keresendő elem
	 * @return igaz, ha az elem megtalálható a tömbben, egyébként hamis
	 */
	public static <T> boolean arrayContainsElement(T[] array, T element) {
		if (array == null || element == null) return false;
	    boolean contains = false;
	    for (int i = 0; !contains && i < array.length; ++i) contains = array[i].equals(element);
	    return contains;
	}

	/**
	 * Megszámolja, hogy egy tömbben hányszor találhatóak meg a listában adott elemek
	 * @param <T> a tömb és az elemek típusa
	 * @param array a tömb, amiben az elemet kell megszámolni
	 * @param elements a lista, ami a megszámolandó elemeket tartalmazza
	 * @return a tömbben található listában lévő elemek száma
	 */
	public static <T> int arrayElementsCount(T[] array, ArrayList<T> elements) {
	    if (elements == null) return 0;
	    int count = 0;
	    for (T e : elements) if (arrayContainsElement(array, e)) ++count;
	    return count;
	}

}