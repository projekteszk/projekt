package core;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;
import misc.Utils;

@XmlRootElement
public class Pizza {

    /**
     * A pizzához használható összetevők
     */
    public enum Osszetevo {
        HUS, SAJT, ZOLDSEG
    }
    
    private String teszta;
    private Integer tesztaMeret; 
    private String alap;  
    private ArrayList<String> hus = new ArrayList<>();
    private ArrayList<String> sajt = new ArrayList<>();
    private ArrayList<String> zoldseg = new ArrayList<>();

    // validators
    static final String[] TESZTAK = {"vékony", "vastag", "normál" };
    static final Integer[] MERETEK = {30, 15, 50};
    static final String[] ALAPOK = {"paradicsom", "tejföl", "bbq"};
    static final String[] HUSOK = {"szalámi", "sonka", "kolbász"};
    static final String[] SAJTOK = {"mozzarella", "füstölt sajt", "camambert"};
    static final String[] ZOLDSEGEK = {"oliva", "kukorica", "hagyma", "brokkoli"};
	
    //üres konstruktor, ne töröljétek!
    /**
     * Létrehoz egy üres pizzát.
     */
    public Pizza() { }

    /**
     * Létrehoz egy pizzát a megadott paraméterekkel.
     * @param teszta a pizza tésztája
     * @param tesztaMeret a pizza mérete
     * @param alap a pizza alapja
     * @param hus húsok a pizzára
     * @param sajt sajtok a pizzára
     * @param zoldseg zöldségek a pizzára
     */
    public Pizza(String teszta, Integer tesztaMeret, String alap, ArrayList<String> hus, ArrayList<String> sajt, ArrayList<String> zoldseg) {
        this.teszta = teszta;
        this.tesztaMeret = tesztaMeret;
        this.alap = alap;
        this.hus = hus;
        this.sajt = sajt;
        this.zoldseg = zoldseg;
    }

    /**
     * Létrehoz egy pizzát a megadott tésztával, mérettel, alappal és összetevőkkel.
     * @param teszta a pizza tésztája
     * @param tesztaMeret a pizza mérete
     * @param alap a pizza alapja
     * @param osszetevok a pizza összetevői
     */
    public Pizza(String teszta, Integer tesztaMeret, String alap, ArrayList<String> osszetevok) {
        this(teszta, tesztaMeret, alap, null, null, null);
        for (String ot : osszetevok) addOsszetevo(ot);
    }

	// updated with getter, setter - team08a (anita)
	/**
     * Visszaadja a pizza tésztáját
     * @return a pizza tésztája
     */
    public String getTeszta() {
        return teszta;
    }

    /**
     * Visszaadja a pizza tésztájának méretét
     * @return a pizza tészta mérete
     */
    public Integer getTesztaMeret() {
        return tesztaMeret;
    }

    /**
     * Visszaadja a pizza alapját
     * @return a pizza alapja
     */
    public String getAlap() {
        return alap;
    }

    /**
     * Visszaadja a pizza húsait
     * @return a pizza húsai
     */
    public ArrayList<String> getHus() {
        return hus;
    }

    /**
     * Visszaadja a pizza sajtjait
     * @return a pizza sajtjai
     */
    public ArrayList<String> getSajt() {
        return sajt;
    }

    /**
     * Visszaadja a pizza zöldségeit
     * @return a pizza zöldségei
     */
    public ArrayList<String> getZoldseg() {
        return zoldseg;
    }

    /**
     * Beállítja a pizza tésztáját
     * @param teszta a pizza tésztája
     */
    public void setTeszta(String teszta) {
        this.teszta = teszta;
    }

    /**
     * Beállítja a pizza tésztájának méretét
     * @param tesztaMeret a pizza tésztájának mérete
     */
    public void setTesztaMeret(Integer tesztaMeret) {
        this.tesztaMeret = tesztaMeret;
    }

    /**
     * Beállítja a pizza alapját
     * @param alap a pizza alapja
     */
    public void setAlap(String alap) {
        this.alap = alap;
    }

    /**
     * Beállítja a pizza húsait
     * @param hus a pizza húsai
     */
    public void setHus(ArrayList<String> hus) {
        this.hus = hus;
    }

    /**
     * Beállítja a pizza sajtjait
     * @param sajt a pizza sajtjai
     */
    public void setSajt(ArrayList<String> sajt) {
        this.sajt = sajt;
    }

    /**
     * Beállítja a pizza zöldségeit
     * @param zoldseg a pizza zöldségei
     */
    public void setZoldseg(ArrayList<String> zoldseg) {
        this.zoldseg = zoldseg;
    }

    // updated with isValid - team08c (attila)
    /**
     * Megvizsgálja, hogy egy pizza érvényes adatokat tartalmaz-e.
     * @return igaz, ha minden adat helyes, egyébként hamis.
     */
    public boolean isValid() {
        return Utils.arrayContainsElement(TESZTAK, teszta) && Utils.arrayContainsElement(MERETEK, tesztaMeret) && Utils.arrayContainsElement(ALAPOK, alap) && 
            (Utils.arrayElementsCount(HUSOK, hus) > 0 || Utils.arrayElementsCount(SAJTOK, sajt) > 0 || Utils.arrayElementsCount(ZOLDSEGEK, zoldseg) > 0);
    }
    
    /**
     * Létrehoz egy pizzát az adott összetevőkből
     * @param osszetevok a pizza összetevői
     * @deprecated Nem lehet teljes pizzát létrehozni vele, helyette {@link #Pizza(java.lang.String, java.lang.Integer, java.lang.String, java.util.ArrayList) }
     */
    @Deprecated
    public Pizza(ArrayList<String> osszetevok) {
        
    }

    /**
     * Megállapítja egy összetevőröl, hogy melyik típusba tartozik
     * @param összetevo az összetevő
     * @return az összetevő típusa
     */
    private Osszetevo getOsszetevoTipus(String osszetevo) {
        for (String ot : HUSOK) if (ot.equals(osszetevo)) return Osszetevo.HUS;
        for (String ot : SAJTOK) if (ot.equals(osszetevo)) return Osszetevo.SAJT;
        for (String ot : ZOLDSEGEK) if (ot.equals(osszetevo)) return Osszetevo.ZOLDSEG;
        return null;
    }
    
    /**
     * Hozzáad egy összetevőt a meglévőkhöz
     * @param osszetevo az összetevő
     */
    private void addOsszetevo(String osszetevo) {
        switch (getOsszetevoTipus(osszetevo)) {
            case HUS: hus.add(osszetevo);
                break;
            case SAJT: sajt.add(osszetevo);
                break;
            case ZOLDSEG: zoldseg.add(osszetevo);
                break;
        }
    }

    @Override
    public String toString(){
        return this.teszta + " " + this.tesztaMeret + " cm, " 
            + this.alap + " alap, feltét: " + 
                this.hus + this.sajt + this.zoldseg;
    }
}