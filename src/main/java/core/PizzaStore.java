package core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


public class PizzaStore {

    static final String PIZZAK = "pizzak.pizza";
    static final String ARAK = "arak.pizza";
	
	/**
     * 
     * TODO jobb lenne valamilyen JSON parsert találni (a JSONObject/JSONArray nekem nem működött)
     * minta: {teszta: 'vekony', tesztaMeret : 25, alap : 'tejfol', hus: ['sonka'], sajt : ['mozarella', 'fustolt'] , zoldseg : ['oliva', 'kukorica']}
     * @param p 
     * @throws java.io.IOException 
     */
    public static void savePizza(Pizza p) throws IOException{
        String hus     = getJson(p.getHus());
        String sajt    = getJson(p.getSajt());
        String zoldseg = getJson(p.getZoldseg());
        String json = "minta: {teszta: '" + p.getTeszta() + "', " +
                      "tesztaMeret: " + Integer.toString(p.getTesztaMeret()) + ", " +
                      "alap: " + p.getAlap() + ", " +
                      "hus: [" + hus + "]," +
                      "sajt: [" + sajt + "]," +
                      "zoldseg: [" + zoldseg + "]}";
        
        File file = new File("PIZZAK");
 
        if(!file.exists()){
            file.createNewFile();
        }

        //true = append file
        FileWriter fileWritter = new FileWriter(file.getName(),true);
        try (BufferedWriter bufferWritter = new BufferedWriter(fileWritter)) {
            bufferWritter.write(json);
        }
    }
    
    /**
     * TODO jobb lenne valamilyen JSON parsert találni (a JSONObject/JSONArray nekem nem működött)
     * Sorrendfüggő! (a fenti minta alapján)
     * @return
     * @throws java.io.IOException
     */
    public ArrayList<Pizza> loadPizzas() throws IOException {
        ArrayList<Pizza> pizzak = new ArrayList<>();
        
        try (BufferedReader bufferReader = new BufferedReader(new FileReader("PIZZAK"))) {
            String line;
            while ((line = bufferReader.readLine()) != null) {
                line = line.replaceAll("\\s","");
                line = line.replaceAll("'", "");
                String name = line.substring(0, line.indexOf(":"));
                String teszta = line.substring(line.indexOf("teszta:")+7, line.indexOf(",tesztaMeret:"));
                String tesztameret = line.substring(line.indexOf("tesztaMeret:")+12, line.indexOf(",alap:"));
                String alap = line.substring(line.indexOf("alap:")+5, line.indexOf(",hus:"));
                String hus = line.substring(line.indexOf("hus:")+4, line.indexOf(",sajt:"));
                String sajt = line.substring(line.indexOf("sajt:")+5, line.indexOf(",zoldseg:"));
                String zoldseg = line.substring(line.indexOf("zoldseg:")+8, line.indexOf("}"));
                
                ArrayList<String> husarr = getFromJson(hus);
                ArrayList<String> sajtarr = getFromJson(sajt);
                ArrayList<String> zoldsegarr = getFromJson(zoldseg);
                
                pizzak.add(new Pizza(teszta, Integer.parseInt(tesztameret), alap, husarr, sajtarr, zoldsegarr));
            }
        }
        return pizzak;
    }
	
	
	//updated with calculate functions (team08a - anita)
    
    /**
     * TODO
     * @param p
     * @return
     * kezdetben a price azért 100, mert a pizza alapokat ugyanannyinak veszem.
     * a tészta méreteitől függően már eltérnek a hozzáadott árak
     * minél több feltét van, annál többet adok hozzá
     */
    public static int calculate(Pizza p) {
        int price = 100;
        
        if (p.getTesztaMeret() == 15) {
            price += 500;
        } else if (p.getTesztaMeret() == 30) {
            price += 800;
        } else if (p.getTesztaMeret() == 50) {
            price += 900;
        }
        
        int feltethus;
        int feltetsajt;
        int feltetzoldseg;
        
        if (p.getHus() == null) {
            feltethus = 0;
        }
        else {
            feltethus = p.getHus().size();
        }
        
        if (p.getSajt()== null) {
            feltetsajt = 0;
        }
        else {
            feltetsajt = p.getSajt().size();
        }
        
        if (p.getZoldseg()== null) {
            feltetzoldseg = 0;
        }
        else {
            feltetzoldseg = p.getZoldseg().size();
        }
        
        int feltetek = feltethus + feltetsajt + feltetzoldseg;
        price += feltetek * 100;
        
        
        return price;
    }
    
    /**
     *
     * @param pizzas
     * @return
     */
    public int calculate(ArrayList<Pizza> pizzas) {
        int cost = 0;
        for (Pizza p : pizzas) cost += calculate(p);
        return cost;
    }
    
    private static String getJson(ArrayList<String> str) {
        String ret = "";
        for(String tmp : str) {
            ret += "'" + tmp + "',";
        }
        return ret.substring(0, ret.length()-1);
    }
    
    private static ArrayList<String> getFromJson(String str) {
        ArrayList<String> ret = new ArrayList<>();
        str = str.replaceAll("[", "");
        str = str.replaceAll("]", "");
        String[] tokens = str.split(",");
        ret.addAll(Arrays.asList(tokens));
        return ret;
    }
}