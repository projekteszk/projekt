var APP = {};

APP.addPizzaURL = window.location.href + "rest/p/add";
//alert(APP.addPizzaURL);

/* set up UI */
APP.pizzaForm = document.getElementById('pizzaForm');
APP.addButton = document.getElementById('addButton');
APP.pizzaDisplays = document.getElementById('pizzaDisplays');

APP.tesztaSelect = document.getElementById('tesztaSelect');
APP.meretRadio = document.getElementsByName('meretRadio');
APP.alapRadio = document.getElementsByName('alapRadio');
APP.husChk = document.getElementsByName('husChk');
APP.sajtChk = document.getElementsByName('sajtChk');
APP.zoldsegChk = document.getElementsByName('zoldsegChk');

APP.pizzaPanel = document.getElementById('pizzaPanel');
APP.pizzaPanelBody = document.getElementById('pizzaPanelBody');
APP.pizzaTabla = document.getElementById('pizzaTabla');

APP.errorMessage = document.getElementById('errorMessage');

APP.spinHolder = document.getElementById('spinHolder');

/* setup spinner */
APP.spinnerOpts = {
  lines: 9, // The number of lines to draw
  length: 10, // The length of each line
  width: 5, // The line thickness
  radius: 10, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '150px', // Top position relative to parent
  left: '50%' // Left position relative to parent
};
var spinner = new Spinner(APP.spinnerOpts).spin(APP.spinHolder);

APP.getSelectedToppings = function(name){
	var selected = [];
	var chkName = name + "Chk";
	for (var i = 0, length = APP[chkName].length; i < length; i++) {
    	if (APP[chkName][i].checked) {
        	selected.push(APP[chkName][i].value);
    	}
    } 
	return selected;
}

APP.getSelectedBase = function(name){
	var radioName = name + "Radio";
	var selected;
	for (var i = 0, length = APP[radioName].length; i < length; i++){
    	if (APP[radioName][i].checked) {
        	selected = APP[radioName][i].value;
        	break;
    	}
    }
    return selected;
}

APP.getPizzaForm = function(){

	var pizza = {};
	pizza.teszta = APP.tesztaSelect.value;
	pizza.tesztaMeret = APP.getSelectedBase('meret');
	pizza.alap = APP.getSelectedBase('alap');

	pizza.hus = [];
	pizza.sajt = [];
	pizza.zoldseg = [];

    pizza.hus = APP.getSelectedToppings('hus');
    pizza.sajt = APP.getSelectedToppings('sajt'); 
    pizza.zoldseg = APP.getSelectedToppings('zoldseg');  

	return pizza;
}

APP.addButton.addEventListener('click', function( e ){
	e.preventDefault();
	APP.errorMessage.style.display = 'none';
	var pizza = APP.getPizzaForm();
	APP.addPizza(pizza);
	return false;
});

APP.handleResponse = function(response){
	if(response.status === "ok" ){
		console.log("status ok");
		pizza = APP.getPizzaForm();
		var row = APP.pizzaTabla.insertRow(0);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		cell1.innerHTML = APP.getPizzaStringForDisplay();
		cell2.innerHTML = response.price;
	}
	else{
		console.log("status invalid");
		APP.errorMessage.style.display = 'block';
	}

	APP.spinHolder.style.visibility = "hidden";
	APP.pizzaDisplays.style.visibility = "visible";
}

APP.addPizza = function( pizza ){

	APP.pizzaDisplays.style.visibility = "hidden";
	APP.spinHolder.style.visibility = "visible";

	var xmlhttp = new XMLHttpRequest();
	var response = {};

	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			response = JSON.parse(xmlhttp.responseText);
			APP.handleResponse(response);
		}
	}
	xmlhttp.open("POST",APP.addPizzaURL,true);
	xmlhttp.setRequestHeader("Content-type","application/json");
	xmlhttp.send(JSON.stringify(pizza));
}

APP.updatePizzaPanel = function(){
	
	var displayStr = APP.getPizzaStringForDisplay();
	APP.pizzaPanelBody.innerHTML = "<span>"+ displayStr +"</span>";

} 

APP.getPizzaStringForDisplay = function(){

	var pizza = APP.getPizzaForm();
	console.log(pizza);

	var feltetStr = "";
	for (var i = 0; i < pizza.hus.length; i++) {
		feltetStr += pizza.hus[i] + ", ";
	};
	for (var i = 0; i < pizza.sajt.length; i++) {
		feltetStr += pizza.sajt[i] + ", ";
	};
	for (var i = 0; i < pizza.zoldseg.length; i++) {
		feltetStr += pizza.zoldseg[i] + ", ";
	};
	//szégyen kód :D

	if(feltetStr.length > 0){
		feltetStr = " + " + feltetStr;
		feltetStr = feltetStr.substring(0, feltetStr.length - 2);
	}
	
	return pizza.tesztaMeret + " cm " + pizza.teszta +" tészta " 
			+pizza.alap+" alappal " + feltetStr;
}


onload = function(e) {

    APP.pizzaSelects = document.getElementsByTagName('input');
    for(i=0; i<APP.pizzaSelects.length; i++) {
        APP.pizzaSelects[i].addEventListener('change', APP.updatePizzaPanel , false);
    }

    APP.tesztaSelect.addEventListener('change', APP.updatePizzaPanel , false);
}