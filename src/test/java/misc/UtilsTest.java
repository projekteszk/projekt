package misc;

import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UtilsTest {

    /**
     * Mindkét paraméter null, nincs mit vizsgálni, az eredmény false
     */
    @Test
    public void arrayContainsElement1() {
        assertFalse(Utils.arrayContainsElement(null, null));
    }
    
    /**
     * Az első paraméter null, nincs miben keresni, az eredmény false
     */
    @Test
    public void arrayContainsElement2() {
        assertFalse(Utils.arrayContainsElement(null, 3));
    }
    
    /**
     * A második paraméter null, nincs mit keresni, az eredmény false
     */
    @Test
    public void arrayContainsElement3() {
        assertFalse(Utils.arrayContainsElement(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, null));
    }
    
    /**
     * A tömb hossza 0, a ciklusba nem lép be, az eredmény false
     */
    @Test
    public void arrayContainsElement4() {
        assertFalse(Utils.arrayContainsElement(new Integer[] { }, 3));
    }
    
    /**
     * A tömb hossza nem 0, nincs benne a keresett elem, az eredmény false
     */
    @Test
    public void arrayContainsElement5() {
        assertFalse(Utils.arrayContainsElement(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, 8));
    }
    
    /**
     * A tömb hossza nem 0, egyszer van benne a keresett elem, az eredmény true
     */
    @Test
    public void arrayContainsElement6() {
        assertTrue(Utils.arrayContainsElement(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, 5));
    }
    
    /**
     * A tömb hossza nem 0, kétszer van benne a keresett elem, az eredmény true
     */
    @Test
    public void arrayContainsElement7() {
        assertTrue(Utils.arrayContainsElement(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, 4));
    }
    
    /**
     * Mindkét paraméter null, nincs mit vizsgálni, az eredmény 0
     */
    @Test
    public void arrayElementsCount1() {
        assertEquals(0, Utils.arrayElementsCount(null, null));
    }
    
    /**
     * Az első paraméter null, nincs miben keresni, az eredmény 0
     */
    @Test
    public void arrayElementsCount2() {
        assertEquals(0, Utils.arrayElementsCount(null, new ArrayList<Integer>(Arrays.asList(2, 4, 5))));
    }
    
    /**
     * A második paraméter null, nincs mit keresni, az eredmény 0
     */
    @Test
    public void arrayElementsCount3() {
        assertEquals(0, Utils.arrayElementsCount(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, null));
    }
    
    /**
     * A lista nem tartalmaz elemet, az eredmény 0
     */
    @Test
    public void arrayElementsCount4() {
        assertEquals(0, Utils.arrayElementsCount(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, new ArrayList<Integer>()));
    }
    
    /**
     * A listában csak olyan elem van, ami nincs a tömbben, az eredmény 0
     */
    @Test
    public void arrayElementsCount5() {
        assertEquals(0, Utils.arrayElementsCount(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, new ArrayList<Integer>(Arrays.asList(6, 8, 9))));
    }
    
    /**
     * A listában csak olyan elem van, ami a tömbben is, az eredmény 1 + 1 + 0 = 3
     */
    @Test
    public void arrayElementsCount6() {
        
        assertEquals(3, Utils.arrayElementsCount(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, new ArrayList<Integer>(Arrays.asList(2, 4, 5))));
    }
    
    /**
     * A listában vegyesen vannak az elemek, az eredemény 1 + 0 + 0 + 1 + 1 + 0 = 3
     */
    @Test
    public void arrayElementsCount7() {
        assertEquals(3, Utils.arrayElementsCount(new Integer[] { 2, 3, 4, 5, 2, 3, 4 }, new ArrayList<Integer>(Arrays.asList(2, 6, 8, 4, 5, 9))));
    }
    
}