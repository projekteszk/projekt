package core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class PizzaTest{

    /**
    *	Feltét nélküli pizza invalid kell, hogy legyen 
    */
    @Test
    public void isValid1(){
        Pizza p = new Pizza("vékony",30,"tejföl",null,null,null);
        assertFalse(p.isValid());
    }

    /**
    *	Legalább egy feltétet tartalmazó már valid (hus)
    */
    @Test
    public void isValid2(){
        ArrayList<String> hus = new ArrayList<>();
        hus.add("sonka"); 
        Pizza p = new Pizza("vékony",30,"tejföl",hus,null,null);
        assertTrue(p.isValid());
    }

    /**
    *	Nem helyes tészta esetén invalid
    */
    @Test
    public void isValid3(){
        ArrayList<String> hus = new ArrayList<>();
        hus.add("sonka"); 
        Pizza p = new Pizza("fake",30,"tejföl",hus,null,null);
        assertFalse(p.isValid());
    }
    
    /**
    *	Nem helyes tésztaméret esetén invalid
    */
    @Test
    public void isValid4(){
        ArrayList<String> hus = new ArrayList<>();
        hus.add("sonka"); 
        Pizza p = new Pizza("vékony",100,"tejföl",hus,null,null);
        assertFalse(p.isValid());
    }
    
    /**
    *	Nem helyes alap esetén invalid
    */
    @Test
    public void isValid5(){
        ArrayList<String> hus = new ArrayList<>();
        hus.add("sonka"); 
        Pizza p = new Pizza("vékony",30,"fake",hus,null,null);
        assertFalse(p.isValid());
    }
    
    /**
    *	Mindenből egy feltét esetén valid
    */
    @Test
    public void isValid6(){
        Pizza p = new Pizza("vékony",30,"tejföl",
            new ArrayList<>(Arrays.asList("sonka")),
            new ArrayList<>(Arrays.asList("mozarella")),
            new ArrayList<>(Arrays.asList("oliva"))
        );
        assertTrue(p.isValid());
    }
    
    /**
    *	Minden létező feltét esetén valid
    */
    @Test
    public void isValid7(){
        Pizza p = new Pizza("vékony",30,"tejföl",
            new ArrayList<>(Arrays.asList(Pizza.HUSOK)),
            new ArrayList<>(Arrays.asList(Pizza.SAJTOK)),
            new ArrayList<>(Arrays.asList(Pizza.ZOLDSEGEK))
        );
        assertTrue(p.isValid());
    }
    
}