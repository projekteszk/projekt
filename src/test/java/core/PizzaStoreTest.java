package core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PizzaStoreTest{

	/**
     * A pizza mérete 15, van rajta 1 feltét -> eredmény 700
     */
	@Test
    public void shouldCalculateFairPrice() {
		ArrayList<String> sajt = new ArrayList<>();
        sajt.add("mozarella");

		Pizza piz = new Pizza("vékony",15,"paradicsom",null,sajt,null);
		
        assertEquals(700, PizzaStore.calculate(piz));
    }

	/**
     * A pizza mérete 30, van rajta 1 feltét -> eredmény 1000
     */
    @Test
    public void shouldCalculateFairPrice2() {
		ArrayList<String> hus = new ArrayList<>();
        hus.add("sonka");

		Pizza piz = new Pizza("vékony",30,"bbq",hus,null,null);
		
        assertEquals(1000, PizzaStore.calculate(piz));
    }
	
	/**
     * A pizza mérete 50, van rajta 1 feltét -> eredmény 1100
     */
    @Test
    public void shouldCalculateFairPrice3() {
		ArrayList<String> zoldseg = new ArrayList<>();
        zoldseg.add("kukorica");

		Pizza piz = new Pizza("vékony",50,"tejföl",null,null,zoldseg);
		
        assertEquals(1100, PizzaStore.calculate(piz));
    }
	
	/**
     * A pizza mérete 50, van rajta 11 feltét -> eredmény 2100
     */
    @Test
    public void shouldCalculateFairPrice4() {
		Pizza piz = new Pizza();
		
		piz.setTesztaMeret(50);
		piz.setSajt(new ArrayList<>(Arrays.asList("mozarella", "füstölt", "camambert")));
		piz.setHus(new ArrayList<>(Arrays.asList("szalámi", "sonka", "kolbász")));
		piz.setZoldseg(new ArrayList<>(Arrays.asList("oliva", "paradicsom", "kukorica", "hagyma", "brokkoli")));
		
        assertEquals(2100, PizzaStore.calculate(piz));
    }
	

}